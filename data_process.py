import cv2
import numpy as np
import pickle
import pandas as pd

#mapping numbers to corresponding categories
cat = {   
    'antelope' : 0,
    'bat' : 1,
    'beaver' : 2,
    'bobcat' : 3,
    'buffalo' : 4,
    'chihuahua' : 5,
    'chimpanzee' : 6,
    'collie' : 7,
    'dalmatian' : 8,
    'german+shepherd' : 9,
    'grizzly+bear' : 10,
    'hippopotamus' : 11,
    'horse' : 12,
    'killer+whale' : 13,
    'mole' : 14,
    'moose' : 15,
    'mouse' : 16,
    'otter' : 17,
    'ox' : 18,
    'persian+cat' : 19,
    'raccoon' : 20,
    'rat' : 21,
    'rhinoceros' : 22,
    'seal' : 23,
    'siamese+cat' : 24,
    'spider+monkey' : 25,
    'squirrel' : 26,
    'walrus' : 27,
    'weasel' : 28,
    'wolf' : 29
    }
#data-set lengths
train_n = 13000
test_n = 6000

data = {}
train_x = np.zeros((train_n,100*100*3))
train_y = np.zeros((train_n))
test_x = np.zeros((test_n,100*100*3))


#function for loading data and rescaling it
def load_rescale(name):
    img = cv2.imread(name)
    img = cv2.resize(img, dsize=(100, 100), interpolation=cv2.INTER_CUBIC)
    return img.flatten()

def pickle_data(fname):
    file = open(fname,'wb')
    pickle.dump(data,file)
    return

train_data = pd.read_csv('train.csv')

for i in range(train_n):
    x = load_rescale('train\\' + str(train_data["Image_id"][i]))
    y = cat[str(train_data["Animal"][i])]
    train_x[i] = x
    train_y[i] = y
    print('Done with train image',i+1)

data["features"] = train_x
data["labels"] = train_y
pickle_data('train_data.pickle')

test_data = pd.read_csv('test.csv')

for i in range(test_n):
    x = load_rescale('test\\' + str(test_data["Image_id"][i]))
    test_x[i] = x
    print('Done with test image',i+1)

data["features"] = test_x
pickle_data('test_data.pickle')



