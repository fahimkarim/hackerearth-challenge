# HackerEarth Challenge

This repository contains a Convolutional Neural Network which classifies animals according to 30 different categories.
[Link to HackerEarth Challenge](https://www.hackerearth.com/challenge/competitive/deep-learning-beginner-challenge/problems/)

The pictures were converted to __100x100x3__ images and then unrolled into a single numpy vector. These vectors were saved using Pickle.

The ___data_process.py___ contains the code necessary to generate input data.
___cnn.py___ contains the Tensorflow code which is the CNN model.
Use ___test.py___ to randomly select a data from train_data.pickle and save it as __test_img.jpg__. This helps to check the authenticity of the train data
and to remove possible errors due to mismatch of dimensions.

The pickle files are too large for storage so they have been moved to google drive.
[Link to Google Drive](https://drive.google.com/open?id=1erlx2EuEbZ6W2wR51ZN_CgFj88k5waR7)