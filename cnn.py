from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from datetime import datetime

import numpy as np
import tensorflow as tf
import pickle

def unpickle(filename):
    file = open(filename,'rb')
    dict = pickle.load(file)
    dict['features'] = dict['features'].astype('float32')
    dict['labels'] = dict['labels'].astype('int32')

    return dict

tf.logging.set_verbosity(tf.logging.INFO)

def cnn_model_fn(features,labels,mode):

    input_layer = tf.reshape(features['features'],[-1,100,100,3])

    conv1 = tf.layers.conv2d(
        inputs = input_layer,
        filters = 32,
        kernel_size = [5,5],
        padding = "same",
        activation = tf.nn.relu
    )

    pool1 = tf.layers.max_pooling2d(
        inputs = conv1,
        pool_size = [2,2],
        strides = 1
    )

    conv2 = tf.layers.conv2d(
        inputs = pool1,
        filters = 64,
        kernel_size = [5,5],
        padding = "same",
        activation = tf.nn.relu
    )

    pool2 = tf.layers.max_pooling2d(
        inputs = conv2,
        pool_size = [2,2],
        strides = 2
    )

    pool2_flat = tf.reshape(pool2,[-1,49*49*64])
    dense = tf.layers.dense(
        inputs = pool2_flat,
        units = 500,
        activation = tf.nn.relu
    )
    dropout = tf.layers.dropout(
        inputs = dense,
        rate = 0.4,
        training = mode==tf.estimator.ModeKeys.TRAIN
    )
    logits = tf.layers.dense(
        inputs = dropout,
        units = 30
    )
    
    predictions = {
        'classes' : tf.argmax(input=logits, axis=1),
        'probabilities' : tf.nn.softmax(logits, name="softmax_tensor")
    }

    if(mode == tf.estimator.ModeKeys.PREDICT):
        return tf.estimator.EstimatorSpec(mode, predictions = predictions)
    
    onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32),depth=30)
    loss = tf.losses.softmax_cross_entropy(onehot_labels=onehot_labels,logits=logits)

    if(mode == tf.estimator.ModeKeys.TRAIN):
        optimzer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = optimzer.minimize(
            loss=loss,
            global_step = tf.train.get_global_step()
        )

        return tf.estimator.EstimatorSpec(mode,loss=loss,train_op=train_op)

    eval_metric_ops = {
        'accuracy' : tf.metrics.accuracy(labels=labels, predictions=predictions["classes"])
    }
    return tf.estimator.EstimatorSpec(mode,loss=loss, eval_metric_ops=eval_metric_ops)

def main(_):
    cnn_classifier = tf.estimator.Estimator(model_fn=cnn_model_fn,model_dir="tmp/cnn_classifier")
    
    data = unpickle('train_data.pickle')
    train_x = data["features"][0:10000]
    train_y = data["labels"][0:10000]
    test_x = data["features"][10000:]
    test_y = data["labels"][10000:]
    
    tensors_to_log = {'probabilities' : 'softmax_tensor'}
    logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log,every_n_iter=100)
    
    print("Start time : ",datetime.now().time())
    
    print("-------Beginning Training-------")
    
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x = {"features" : train_x},
            y = train_y,
            num_epochs = None,
            batch_size = 100,
            shuffle = True
            )
    
    cnn_classifier.train(
            input_fn = train_input_fn,
            steps = 10000,
            hooks=[logging_hook]
            )    


    print("-------Finished Training-------")

    print("-------Beginning testing-------")
    test_input_fn = tf.estimator.inputs.numpy_input_fn(
        x = {"features" : test_x},
        y = test_y,
        num_epochs = 1,
        batch_size = 100,
        shuffle = False
    )

    results = cnn_classifier.evaluate(input_fn = test_input_fn)
    print("Test results: ",results)

if __name__ == "__main__":
    tf.app.run()