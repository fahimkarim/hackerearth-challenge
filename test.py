import cv2
import numpy as np
import pickle
import random

f = open('train_data.pickle','rb')
data = pickle.load(f,encoding='bytes')
img = random.choice(data['features'])
print(img.shape)
img = img.reshape((100,100,3))
cv2.imwrite('test_img.jpg',img)
